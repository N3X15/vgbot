"""
Adapted from http://sprunge.us/iFQc?python

Thanks to mloc.
"""
from vgstation.common.plugin import IPlugin, Plugin
import vgstation.common.config as globalConfig
import logging
import random
import re
import time
import json

BUG_CHECK_DELAY = 60  # 60sec

REG_PATH = re.compile(r"\[([a-zA-Z\-_/][a-zA-Z0-9\- _/]*\.[a-zA-Z]+)\]", re.I)
REG_ISSUE = re.compile(r"\[#?([0-9]+)\]")
REG_COMMIT = re.compile(r"\[#?([a-zA-Z0-9]+)\]")

TREE_CHECK_DELAY = 300  # 5 minutes

REPLY_LIMIT = 5


@Plugin
class GitHubPlugin(IPlugin):

    def __init__(self, bot):
        import github3
        import psutil

        IPlugin.__init__(self, bot)

        self.data = None
        self.config = None
        self.url = None
        self.ignored = []
        self.auth = None
        self.project_id = None
        self.resource = None
        self.lastCheck = 0

        self.tree = None
        self.nextTreeDownload = 0
        self.tree_dict = None

        self.config = globalConfig.get('plugins.github')
        if self.config is None:
            logging.warn('GitHub: Disabled.')
            return

        self.data = {
            'last-bug-created': 0,
            'ignored-names': [
                '/^Not\-[0-9]+/'  # Notifico bots
            ]
        }

        self.LoadPluginData()

        self.url = globalConfig.get('plugins.github.url', None)
        if self.url is None:
            logging.error('GitHub: Disabled.')
            return
        # http://github.com/user/repo
        repodata = self.url[18:]
        if repodata.startswith('/'):
            repodata = repodata[1:]
        repoChunks = repodata.split('/')
        self.user_id = repoChunks[0]
        self.repo_id = repoChunks[1]

        self.ignored = []
        for ignoretok in self.data.get('ignored-names', ['/^Not\-[0-9]/']):
            if ignoretok.startswith('/') and ignoretok.endswith('/'):
                self.ignored += [re.compile(ignoretok[1:-1])]
            else:
                self.ignored += [re.compile('^' + re.escape(ignoretok) + '$')]

        self.bug_info_format = globalConfig.get('plugins.github.bug-info-format', 'GitHub #{ID}: \'{SUBJECT}\' - {URL} ({STATUS})')

        #auth_user = globalConfig.get('plugins.github.username', None)
        auth_key = globalConfig.get('plugins.github.apikey', None)
        self.github = github3.login(token=auth_key)
        self.repo = self.github.repository(self.user_id, self.repo_id)
        self.default_branch = globalConfig.get('plugins.github.default_branch', 'master')

        self.getTree()

    def getTree(self):
        if self.nextTreeDownload < time.time():
            self.nextTreeDownload = time.time() + TREE_CHECK_DELAY
            self.tree = self.repo.tree("HEAD").recurse()
            self.tree_dict = self.tree.as_dict()

    def findPath(self, path):
        pattern = re.compile("^.*(^|/){}$".format(path), re.I)

        for entry in self.tree_dict["tree"]:
            if pattern.match(entry["path"]):
                return(entry["path"])

    def checkIgnore(self, nick):
        for ignored in self.ignored:
            m = ignored.search(nick)
            if m is not None:
                return True
        return False

    def issueToString(self, issue):
        bugmsg = self.bug_info_format
        bugmsg = bugmsg.replace('{ID}', str(issue.number))
        bugmsg = bugmsg.replace('{AUTHOR}', issue.user.login)
        bugmsg = bugmsg.replace('{SUBJECT}', issue.title)
        bugmsg = bugmsg.replace('{STATUS}', issue.state)
        bugmsg = bugmsg.replace('{URL}', issue.html_url)
        return bugmsg.split('{CRLF}')

    def findCommit(self, event):
        replies = []
        #logging.info('got argument <{0}: length{1}>'.format(event.arguments[0], len(event.arguments[0])))
        if len(event.arguments[0]) == 42:
            #logging.warn('DEBUG: found a sha commit')
            matches = REG_COMMIT.finditer(event.arguments[0])
            for match in matches:
                sha = [match.group(1)]
                commit = None
                try:
                    commit = self.repo.git_commit(match.group(1))
                except ConnectionError:
                    self.bot.privmsg(channel, "Unable to connect to the github API at this time.")
                    return None
                #logging.warn('Found hash ({0}) and commit {1}'.format(sha,commit))
                if commit is None or not commit.sha:
                    return replies
                dict = commit.as_dict()
                author = commit._author_name.replace("\n", " ")
                author = author.replace("\r", " ")
                message = dict['message'].replace("\n", " ")
                message = message.replace("\r", " ")
                if not dict['message'] or not dict['author'] or not dict['html_url']:
                    return replies
                logging.warn('commit info as follows: html_url({0}) author({1}) message({2})'.format(author, message, dict['html_url']))

                replies += ['{0} authored {1} ({2})'.format(author, message, dict['html_url'])]
                #sha = (sha[:6] if len(sha) > 7 else sha)
        #if len(replies) < 1: logging.info("we did not find anything arg length {0}".format(len(event.arguments[0])))
        return replies

    def findBugs(self, event):
        ids = []
        for arg in event.arguments:
            logging.debug('ARGS ' + arg)
            matches = REG_ISSUE.finditer(arg)

            for match in matches:
                for group in match.groups():
                    ids += [group]
                    logging.debug('GROUP ' + group)

        replies = []
        for id in ids:
            issue = self.repo.issue(id)
            if issue:
                replies += self.issueToString(issue)
        return replies

    def findPaths(self, event):
        matches = REG_PATH.finditer(event.arguments[0])
        ids = []
        for match in matches:
            ids += [match.group(1)]

        replies = []
        for id in ids:
            path = self.findPath(id)
            if path:
                replies += ['{}/blob/{}/{}'.format(self.url, self.default_branch, path)]
        return replies

    def OnChannelMessage(self, connection, event):
        if self.data is None:
            return

        channel = event.target

        if self.checkIgnore(event.source.nick):
            return

        replies = []
        replies += self.findCommit(event)
        replies += self.findBugs(event)
        replies += self.findPaths(event)

        if len(replies) > 0:
            i = 0
            for reply in replies:
                if reply is None or reply.strip() == '':
                    continue
                i += 1
                if i > REPLY_LIMIT:
                    self.bot.privmsg(channel, 'More than {} results found, aborting to prevent spam.'.format(REPLY_LIMIT))
                    return
                self.bot.privmsg(channel, reply)
