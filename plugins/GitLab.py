"""
Copypasta from github.py, obviously.

Thanks to mloc.
"""
from vgstation.common.plugin import IPlugin, Plugin
import vgstation.utils as utils
import vgstation.common.config as globalConfig
import logging
import random
import re
import time
from vgstation.utils import merge_dicts
from restkit.resource import Resource
from collections import namedtuple

BUG_CHECK_DELAY = 60  # 60sec

# Linking to a file (experimental, slow)
REG_PATH = re.compile(r"\[([a-zA-Z\-_/][a-zA-Z0-9\- _/]*\.[a-zA-Z]+)\]", re.I)
REG_ISSUE = re.compile(r"\[#?([0-9]+)\]")         # Issues are formatted as [#nnn]
REG_MR = re.compile(r"\[(?:\!|MR?#)([0-9]+)\]")    # GitLab has different syntax for issues. Use [!nnn] or [MR#nnn]
REG_COMMIT = re.compile(r"\[I?#?([a-zA-Z0-9]+)\]")  # For linking to commits.

TREE_CHECK_DELAY = 86400  # 1 day

REPLY_LIMIT = 5

try:
    import simplejson as json
except ImportError:
    import json # py2.6 only

def _json_object_hook(d): return namedtuple('X', d.keys())(*d.values())
def json2obj(data): return json.loads(data, object_hook=_json_object_hook)

class GitLabAPI(Resource):
    def __init__(self, base_url,access_token):
        self.access_token = access_token
        #logging.info('INIT GitLabAPI ({})'.format(base_url))
        super(GitLabAPI, self).__init__(base_url, follow_redirect=True, max_follow_redirect=10)
        
        
    def get(self, path=None, headers=None, params_dict=None, **params):
        """ HTTP GET
        
        - path: string  additionnal path to the uri
        - headers: dict, optionnal headers that will
        be added to HTTP request.
        - params: Optionnal parameterss added to the request.
        """
        #logging.info('GET {} ({})...'.format(path,params))
        params['private_token']=self.access_token
        return super(GitLabAPI, self).get(path,headers,params_dict,**params)
    
    def put(self, path=None, payload=None, headers=None, params_dict=None, **params):
        """ HTTP PUT
        
        see POST for params description.
        """
        #logging.info('PUT {} -> {} ({})...'.format(payload,path,params))
        params['private_token']=self.access_token
        return super(GitLabAPI, self).put(path,payload,headers,params_dict,**params)
    
    def post(self, path=None, payload=None, headers=None, params_dict=None, **params):
        """ HTTP PUT
        
        see POST for params description.
        """
        params['private_token']=self.access_token
        return super(GitLabAPI, self).post(path,payload,headers,params_dict,**params)
    
    def request(self, *args, **kwargs):
        #logging.info('-> {} {}'.format(args,kwargs))
        resp = super(GitLabAPI, self).request(*args, **kwargs)
        bod =resp.body_string()
        #logging.info('<- {}'.format(bod))
        return json2obj(bod)


@Plugin
class GitLabPlugin(IPlugin):

    def __init__(self, bot):

        IPlugin.__init__(self, bot)

        self.data = None
        self.config = None
        self.url = None
        self.ignored = []
        self.auth = None
        self.project_id = None
        self.resource = None
        self.lastCheck = 0

        self.tree = None
        self.nextTreeDownload = 0
        self.prefix = None

        self.config = globalConfig.get('plugins.gitlab')
        if self.config is None:
            logging.warn('GitLab: Disabled.')
            return

        self.prefix = self.config.get('prefix', None)

        if self.config.get('api', None) is None:
            logging.warn('GitLab: Disabled (no API configuration!)')
            return

        if self.prefix is not None:
            # Add a prefix to the expression, if desired.  Adds the prefix right before the opening bracket.
            regprefix = '\\[' + self.prefix + '\\s+'
            REG_PATH = re.compile(REG_PATH.pattern.replace('\\[', regprefix))
            REG_ISSUE = re.compile(REG_ISSUE.pattern.replace('\\[', regprefix))
            REG_MR = re.compile(REG_MR.pattern.replace('\\[', regprefix))
            REG_COMMIT = re.compile(REG_COMMIT.pattern.replace('\\[', regprefix))
            logging.info('GitLab: Expressions recompiled')

        self.data = {
            'last-bug-created': 0,
            'ignored-names': [
                '/^Not\-[0-9]+/'  # Notifico bots
            ]
        }

        self.LoadPluginData()
        self.repo_id = self.config.get('project_id', None)
        self.repo_path = self.config.get('project', None)
        self.repo_namespace = self.config.get('namespace', None)
        if self.repo_id is None or self.repo_path is None or self.repo_namespace is None:
            logging.warn('GitLab: Disabled (no repo identifiers).')
            return

        restkit = utils.try_import('restkit','restkit','GitLab')
        #restkit.set_logging('debug')

        self.ignored = []
        for ignoretok in self.data.get('ignored-names', ['/^Not\-[0-9]/']):
            if ignoretok.startswith('/') and ignoretok.endswith('/'):
                self.ignored += [re.compile(ignoretok[1:-1])]
            else:
                self.ignored += [re.compile('^' + re.escape(ignoretok) + '$')]

        self.approvers = self.config.get('approvers', [])

        self.bug_info_format = globalConfig.get('plugins.gitlab.bug-info-format', '{RESET}{GREEN}GitLab #{ID}:{RESET} \'{SUBJECT}\' {LIGHT_GREEN}- {URL}{RESET} {GREEN}({STATUS}){RESET}')
        self.mr_info_format = globalConfig.get('plugins.gitlab.mr-info-format', '{RESET}{GREEN}GitLab !{ID}:{RESET} \'{SUBJECT}\' {LIGHT_GREEN}- {URL}{RESET} {GREEN}({STATUS}){RESET}')
        self.commit_info_format = globalConfig.get('plugins.gitlab.commit-info-format', '{RESET}{GREEN}GitLab {ID} by {LIGHT_GREEN}{AUTHOR}{GREEN}:{RESET} \'{SUBJECT}\' {LIGHT_GREEN}- {URL}{RESET}')

        api_key = self.config['api']['key']
        self.base_url = globalConfig.get('plugins.gitlab.api.base-url', 'https://gitlab.com')+'/api/v3'

        self.api = GitLabAPI(self.base_url,api_key)

        #self.repo = self.gitlab.project('{}%2F{}'.format(self.repo_namespace, self.repo_path))  # workaround for GitLab3 #23
        self.repo = self.api.get('/projects/{}'.format(self.repo_id))#.format(self.repo_namespace, self.repo_path))  # workaround for GitLab3 #23

        if self.repo is None:
            logging.warn('GitLab: Disabled (unable to find repo %s/%s).', self.repo_namespace, self.repo_path)
            return
        else:
            logging.info('GitLab: Found GitLab project %r. (id: %d)', self.repo.path_with_namespace, self.repo.id)
            logging.info('DATA: %r', self.repo)

        self.default_branch = self.config.get('default_branch', 'master')

        self.RegisterCommand('merge', self.OnMerge, help='Will attempt to merge the specified merge request.')
        #self.RegisterCommand('tag', self.OnTag, help='Will attempt to tag the specified merge request or issue.')

        # self.tree_dirs=set()
        # self.getTree()
        
    def GetMR(self,iid):
        import restkit
        try:
            mrs = self.api.get('/projects/{}/merge_requests'.format(self.repo.id),iid=iid)
            if len(mrs)==0: 
                logging.error('Could not find !%s',iid)
                return None
            if mrs[0].iid == int(iid):
                return mrs[0]
        #except gitlab3.exceptions.ForbiddenRequest:
        except restkit.errors.Unauthorized:   
            logging.error('ForbiddenRequest: Bot does not have access to !%s',iid)
        #except gitlab3.exceptions.ResourceNotFound:
        except restkit.errors.ResourceNotFound:
            logging.error('ResourceNotFound: Could not find !%s',iid)
        return None
        
    def GetIssue(self,iid):
        import restkit
        try:
            issues = self.api.get('/projects/{}/issues'.format(self.repo.id), iid=iid)
            if len(issues)==0: 
                logging.error('Could not find #%s',iid)
                return None
            if issues[0].iid == int(iid):
                return issues[0]
        #except gitlab3.exceptions.ForbiddenRequest:
        except restkit.errors.Unauthorized:
            logging.error('ForbiddenRequest: Bot does not have access to #%s',iid)
        #except gitlab3.exceptions.ResourceNotFound:
        except restkit.errors.ResourceNotFound:
            logging.error('ResourceNotFound: Could not find #%s',iid)
        return None

    def OnMerge(self, event, args):
        import restkit
        channel = event.target
        nick = event.source.nick

        if len(args) < 2:
            self.bot.privmsg(channel, 'The format is: {}, merge !ID [...]'.format(self.bot.connection.get_nickname()))
            return True

        if nick not in self.approvers:
            self.bot.notice(nick, 'You are not set as an approver. Access denied.')
            return True

        for mrID in args[1:]:
            print(mrID)
            if mrID.startswith('!'):
                mrID = mrID[1:]
            mrID = int(mrID)
            self.bot.privmsg(channel, 'Attempting to merge !{}...'.format(mrID))
            found = False
            issue=self.GetMR(mrID)
            if issue is not None:
                try:
                    self.api.post('/projects/{pr_id}/merge_requests/{mr_id}/merge'.format(pr_id=self.repo.id,mr_id=mrID))
                    #issue.merge()
                except restkit.errors.Unauthorized:
                    self.bot.privmsg(channel, 'Bot does not have access to merge !{}'.format(mrID))
                    continue
                try:
                    #issue.post_comment(note='Merge approved by {} in IRC.'.format(nick))
                    comment='Merge approved by {} in IRC.'.format(nick)
                    self.api.post('/projects/{pr_id}/merge_requests/{mr_id}/post_comment'.format(pr_id=self.repo.id,mr_id=mrID), note=comment)
                except restkit.errors.Unauthorized:
                    self.bot.privmsg(channel, 'Bot does not have access to comment on !{}'.format(mrID))
                    continue
                self.bot.privmsg(channel, 'Successfully merged !{}.'.format(mrID))
        return True

    def OnTag(self, event, args):
        channel = event.target
        nick = event.source.nick

        if len(args) < 3:
            self.bot.privmsg(channel, 'The format is: {}, merge !ID [...]'.format(self.bot.connection.get_nickname()))
            return True

        if nick not in self.approvers:
            self.bot.notice(nick, 'You are not set as an approver. Access denied.')
            return True

    def OnSpeak(self, event, args):
        self.dropNudges = False
        self.bot.notice(event.source.nick, 'No longer dropping nudges.')
        return True

    def getTree(self):
        if self.nextTreeDownload < time.time():
            self.nextTreeDownload = time.time() + TREE_CHECK_DELAY
            self.tree = self._recurse_tree([])

    def build_tree(self):
        dirdata = {}
        for entry in self.repo.files(ref_name=self.default_branch):
            logging.info(entry)

    def _recurse_tree(self, path):
        logging.debug('Reading tree in %s ...', '/' + ('/'.join(path)))
        dirdata = {}
        # logging.debug(dir(self.repo))
        entries = list(self.repo.files(ref_name=self.default_branch, path='/'.join(path)))
        entries_to_scan = []
        for entry in entries:
            entrypath = path + [entry.name]
            logging.info(entry)
            key = '/' + '/'.join(entrypath)
            if entry.type == 'tree':
                if entry.id not in self.tree_dirs:
                    self.tree_dirs.add(entry.id)
                    entries_to_scan.append(entrypath)
                continue
            dirdata[key] = {'path': key, 'id': entry.id, 'mode': entry.mode}
        for entrypath in entries_to_scan:
            dirdata = merge_dicts(dirdata, self._recurse_tree(entrypath))
        return dirdata

    def findPath(self, path):
        pattern = re.compile("^.*(^|/){}$".format(path), re.I)
        if self.config.get('enable-path', False):
            return 'Path API is disabled.'
        for entry in self.tree["tree"]:
            if pattern.match(entry["path"]):
                return(entry["path"])

    def checkIgnore(self, nick):
        for ignored in self.ignored:
            m = ignored.search(nick)
            if m is not None:
                return True
        return False

    def issueToString(self, issue):
        bugmsg = self.bug_info_format
        bugmsg = bugmsg.replace('{ID}', str(issue.iid))
        bugmsg = bugmsg.replace('{AUTHOR}', issue.author.username)
        bugmsg = bugmsg.replace('{SUBJECT}', issue.title)
        bugmsg = bugmsg.replace('{STATUS}', issue.state)
        bugmsg = bugmsg.replace('{URL}', '{}/issues/{}'.format(self.repo.web_url, issue.iid))
        return bugmsg.split('{CRLF}')

    def mrToString(self, mr):
        bugmsg = self.mr_info_format
        bugmsg = bugmsg.replace('{ID}', str(mr.iid))
        bugmsg = bugmsg.replace('{AUTHOR}', mr.author.username)
        bugmsg = bugmsg.replace('{SUBJECT}', mr.title)
        bugmsg = bugmsg.replace('{STATUS}', mr.state)
        bugmsg = bugmsg.replace('{URL}', '{}/merge_requests/{}'.format(self.repo.web_url, mr.iid))
        return bugmsg.split('{CRLF}')

    def commitToString(self, commit):
        bugmsg = self.commit_info_format
        bugmsg = bugmsg.replace('{ID}', str(commit.id))
        bugmsg = bugmsg.replace('{AUTHOR}', commit.author_name)
        bugmsg = bugmsg.replace('{SUBJECT}', commit.title)
        bugmsg = bugmsg.replace('{URL}', '{}/commit/{}'.format(self.repo.web_url, commit.id))
        return bugmsg.split('{CRLF}')

    def findBugs(self, event):
        import gitlab3
        matches = REG_ISSUE.finditer(event.arguments[0])
        ids = []
        for match in matches:
            ids += [match.group(1)]

        replies = []
        for iid in ids:
            issue = self.GetIssue(iid)
            if issue is not None:
                replies += self.issueToString(issue)
            else:
                replies.append('Could not find issue #{}.'.format(iid))
        return replies

    def findCommit(self, event):
        replies = []
        from gitlab3.exceptions import ConnectionError
        #logging.info('got argument <{0}: length{1}>'.format(event.arguments[0], len(event.arguments[0])))
        if len(event.arguments[0]) == 42:
            #logging.warn('DEBUG: found a sha commit')
            matches = REG_COMMIT.finditer(event.arguments[0])
            for match in matches:
                sha = [match.group(1)]
                commit = None
                try:
                    commit = self.repo.commit(match.group(1))
                except ConnectionError:
                    return ["Unable to connect to the github API at this time."]
                #logging.warn('Found hash ({0}) and commit {1}'.format(sha,commit))
                if commit is None:
                    return replies
                replies += self.commitToString(commit)
                #sha = (sha[:6] if len(sha) > 7 else sha)
        #if len(replies) < 1: logging.info("we did not find anything arg length {0}".format(len(event.arguments[0])))
        return replies

    def findMRs(self, event):
        import gitlab3
        matches = REG_MR.finditer(event.arguments[0])
        ids = []
        for match in matches:
            ids += [match.group(1)]

        replies = []
        for iid in ids:
            issue = self.GetMR(iid)
            if issue:
                replies += self.mrToString(issue)
            else:
                replies.append('Could not find MR #{}.'.format(iid))
        return replies

    def findPaths(self, event):
        matches = REG_PATH.finditer(event.arguments[0])
        ids = []
        for match in matches:
            ids += [match.group(1)]

        replies = []
        for id in ids:
            path = self.findPath(id)
            if path:
                replies += ['{}/blob/{}/{}'.format(self.repo.web_url, self.default_branch, path)]
        return replies

    def OnChannelMessage(self, connection, event):
        if self.data is None:
            return

        channel = event.target

        if self.checkIgnore(event.source.nick):
            return

        replies = []
        replies += self.findCommit(event)
        replies += self.findMRs(event)
        replies += self.findBugs(event)
        # replies += self.findPaths(event) # /project/#/repository/tree broken as of 8/9/2015

        if len(replies) > 0:
            i = 0
            for reply in replies:
                if reply is None or reply.strip() == '':
                    continue
                i += 1
                if i > REPLY_LIMIT:
                    self.bot.privmsg(channel, 'More than {} results found, aborting to prevent spam.'.format(REPLY_LIMIT))
                    return
                self.bot.privmsg(channel, self.bot.AddColors(reply))
