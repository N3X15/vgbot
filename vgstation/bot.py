import string
import irc.bot
import vgstation.common.config as globalConfig
import logging
import re

# Section from notifico.util.irc
###################################
#: Precompiled regex for matching mIRC color codes.
_STRIP_R = re.compile('\x03(?:\d{1,2}(?:,\d{1,2})?)?', re.UNICODE)

#: Common mIRC color codes.
_colors = dict(
    RESET='\x03',
    WHITE='\x03' + '00',
    BLACK='\x03' + '01',
    BLUE='\x03' + '02',
    GREEN='\x03' + '03',
    RED='\x03' + '04',
    BROWN='\x03' + '05',
    PURPLE='\x03' + '06',
    ORANGE='\x03' + '07',
    YELLOW='\x03' + '08',
    LIGHT_GREEN='\x03' + '09',
    TEAL='\x03' + '10',
    LIGHT_CYAN='\x03' + '11',
    LIGHT_BLUE='\x03' + '12',
    PINK='\x03' + '13'
)


def mirc_colors():
    """
    Returns a dictionary mapping color names to common mIRC color
    codes.
    """
    return _colors


def strip_mirc_colors(msg):
    """
    Strips mIRC color codes from `msg`, returning the new string.
    """
    return _STRIP_R.sub('', msg)
# END notifico


class Bot(irc.bot.SingleServerIRCBot):

    def __init__(self, hostname, config):
        irc.client.ServerConnection.buffer_class = irc.client.buffer.LineBuffer
        logging.info('Starting up.' + repr(config))
        port = config['port']
        nickname = config['nick']

        irc.bot.SingleServerIRCBot.__init__(self, [(hostname, port)], nickname, nickname)

        self.chanconfig = config['channels']

        self.command = {}
        self.plugins = []
        for i in ["ping"]:
            self.connection.add_global_handler(i, getattr(self, "on_" + i), 0)
        self.welcomeReceived = False

        self.messageQueue = []
        self.connection.execute_every(1, self.SendQueuedMessage)

    def GetColors(self):
        return _colors
    
    def AddColors(self, msg):
        for k,v in _colors.items():
            msg=msg.replace('{'+k+'}',v)
        return msg

    def StripColors(self, msg):
        return strip_mirc_colors(msg)

    def SendQueuedMessage(self):
        if len(self.messageQueue) == 0:
            return
        msg = self.messageQueue[0]
        msgtype, target, message = msg
        logging.info('{0} -> {1}: {2}'.format(msgtype, target, self.stripUnprintable(message)))
        if msgtype == 'PRIVMSG':
            self.connection.privmsg(target, message)
        elif msgtype == 'NOTICE':
            self.connection.notice(target, message)
        self.messageQueue.remove(msg)

    def on_join(self, c, e):
        ch = e.target
        nick = e.source.nick
        for plugin in self.plugins:
            if plugin.OnJoin(ch, nick):
                break

    def on_ping(self, c, e):
        for plugin in self.plugins:
            if plugin.OnPing():
                break

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        self.welcomeReceived = True
        for channel, channelconfig in self.chanconfig.items():
            password = channelconfig.get('password', None)
            logging.info('Joining {0}...'.format(channel))
            if password is None:
                c.join(channel)
            else:
                c.join(channel, password)

    def on_privmsg(self, c, e):
        msg = e.arguments[0]
        msg = self.stripUnprintable(msg)
        logging.info('PRIVMSG: <{0}> {1}'.format(e.source.nick, msg))
        self.do_command(e, e.arguments[0])

    def on_pubmsg(self, c, e):
        # logging.info(msg.source)
        msg = e.arguments[0]
        msg = self.stripUnprintable(msg)
        logging.info('PUBMSG: <{0}:{1}> {2}'.format(e.source.nick, e.target, msg))
        handled = False
        for plugin in self.plugins:
            if plugin.OnChannelMessage(c, e):
                handled = True
                break
        if not handled:
            if ',' in msg:
                args = msg.split(',', 1)
                logging.debug(repr(args))
                if len(args) > 1 and args[0] in globalConfig.get('names', []):
                    self.do_command(e, args[1].strip())

        return

    def on_dccmsg(self, c, e):
        c.privmsg('Please speak to me in chat or in a PRIVMSG.')

    def on_dccchat(self, c, e):
        return

    def stripUnprintable(self, msg):
        return filter(lambda x: x in string.printable, msg)

    def notice(self, nick, message):
        self.messageQueue += [('NOTICE', nick, message)]
        # self.connection.notice(nick, message)

    def privmsg(self, nick, message):
        self.messageQueue += [('PRIVMSG', nick, message)]
        # self.connection.privmsg(nick, message)

    def do_command(self, e, cmd):
        nick = e.source.nick
        channel = nick
        if e.target:
            channel = e.target
        args = cmd.split(' ')
        cmd = args[0]
        c = self.connection
        if cmd == 'help':
            self.privmsg(nick, '-- VGBot 1.0 Help: (All commands are accessed with {0}, <command> [args]'.format(c.get_nickname()))
            for name in sorted(self.command.keys()):
                self.privmsg(nick, ' {0}: {1}'.format(name, self.command[name].get('help', 'No help= argument for this command.')))
        elif cmd == 'version':
            self.privmsg(channel, 'VGBot 1.0 - By N3X15 - https://gitlab.com/N3X15/vgbot')  # pls to not change
        elif cmd in self.command:
            self.command[cmd]['handler'](e, args)
        else:
            self.privmsg(channel, 'I don\'t know that command, sorry.  Say "{0}, help" for available commands.'.format(c.get_nickname()))

    def sendToAllFlagged(self, flag, msg, notice=False):
        for channel, chandata in self.chanconfig.items():
            if chandata.get(flag, False) == True:
                if notice:
                    self.notice(channel, msg)
                else:
                    self.privmsg(channel, msg)

    def haveJoined(self, channel):
        return channel in self.channels
