'''
Created on Aug 8, 2015

@author: Rob
'''
# http://stackoverflow.com/questions/38987/how-can-i-merge-two-python-dictionaries-in-a-single-expression
# Yes, it's stupidly simple.  No, I don't like re-inventing wheels.
def merge_dicts(*dict_args):
    '''
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    '''
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def try_import(package, module, plugin=None):
    import imp
    
    try:
        found = imp.find_module(module)
        return imp.load_module(module,*found)
    except ImportError:
        if plugin is not None:
            logging.error('{plugin}: {module} module not installed. Fix by running "pip install {package}" if you want {plugin} support.')
        else:
            logging.error('CORE: {module} module not installed.  Fix by running "pip install {package}."')
        return None